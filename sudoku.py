import os
import sys
from random import randint

NUM_ROWS = 9
NUM_COLUMNS = 9

def generate_sudoku(output_filename):
    """ Generate random digits and save them in txt file """

    with open(output_filename, "w") as f:
        numbers = []
        # Create a 9x9 matrix of numbers
        for j in range(1,10):
            for i in range(1,10):
                numbers.append(str(randint(1,9)))
            # Convert a list into a string of numbers 
            # separated by a single space
            f.write(' '.join(numbers) + "\n")
            # Empty list
            del numbers[:]

def load_sudoku(filename):
    """ Open txt file and validate 9x9 matrix """

    numbers = []
    f = open(filename, "r")
    line = f.readline()
    while line:
        # Remove the '\n' and split line separated
        # by spaces
        numbers.append(line.strip('\n').split(' '))
        line = f.readline()
    f.close()

    # Check if any row has wrong number of number
    if type(numbers) is list and len(numbers) != 9:
        return False

    # Check if digits are numerica values
    try:
        for row in numbers:
            for digit in row:
                tmp = int(digit)
    except ValueError:
        print("*** Found non-numeric value in row: {}".format(row))
        return False

    return numbers

def validate_rows(numbers, repeated):
    """ Search for repeated digits in each row """

    result = True
    # Loop through all rows
    for row_index in range(NUM_ROWS): # [1,2,3,...],[3,4,5,...],...
        column = 0        
        # For each row, loop through all columns
        while column < NUM_COLUMNS:
            # Take a number (n)
            n = numbers[row_index][column] 
            # Count if this number n appears more than one time
            if numbers[row_index].count(n) > 1:
                result = False # Not valid sudoku file                
                # Generate a key to be saved in the repeated 
                # dictionary
                row_key = row_index + 1 
                # Make sure this key was not created yet
                if row_key not in repeated['rows']:
                    repeated['rows'][row_key] = { 'repeated': [] }
                # Make sure the duplicated number was not processed yet
                if n not in repeated['rows'][row_key]['repeated']:
                    repeated['rows'][row_key]['repeated'].append(n)       
            column += 1    

    return result

def validate_columns(numbers, repeated):
    """ Search for repeated digits in each column """

    result = True
    column = 0        
    column_numbers = []
    while column < NUM_COLUMNS:
        # Store column numbers in a single list
        for row_index in range(NUM_ROWS):
            column_numbers.append(numbers[row_index][column])
        # Now search for repeated digits
        for number in column_numbers:
            if column_numbers.count(number) > 1:
                result = False
                column_key = column+1
                # Record repeated digits in a dictionary with key "columns"
                if column_key not in repeated['columns']:
                    repeated['columns'][column_key] = { 'repeated': [] }
                if number not in repeated['columns'][column_key]['repeated']:
                    repeated['columns'][column_key]['repeated'].append(number)                 
        # Empty values for this column for the next loop iteration
        del column_numbers[:]
        column += 1

    return result

def validate_rows_3x3(numbers, repeated):
    """ Validate rows in groups of three """

    result = True
    row = 0
    start = 0
    end = 3
    while row < NUM_ROWS:
        row_values = numbers[row]
        #print("row: {0}".format(row))
        while True:
            three_digits = row_values[start:end]
            #print("3 digits: {0}".format(three_digits))

            # Create a list of duplicates and use set() to 
            # reduce duplicates to a single copy of each one
            duplicates = set([str(n) for n in three_digits if three_digits.count(n) > 1])
            if len(duplicates) > 0:
                result = False # Not a valid sudoku
                duplicates = ', '.join(list(duplicates))

                row_key = row + 1 # +1 because to specify a real row number
                if row_key not in repeated['rows3x3']:
                    repeated['rows3x3'][row_key] = { 'repeated': [] }

                repeated['rows3x3'][row_key]['repeated'].append(duplicates)

            # Jump to the next three digits
            start += 3
            end += 3
            # Make sure you do go beyond the limits
            if end > NUM_COLUMNS: 
                start, end = (0, 3)               
                break # Stop second while loop

        row += 1 # jump to the next row

    return result

def validate_columns_3x3(numbers, repeated):
    """ Validate columns in groups of three """

    result = True
    column = 0
    row = 0
    digits = [] # column digits
    while True:    
        #print("row: {0} column: {1}".format(row, column))
        number = numbers[row][column]
        #print("number: {0}".format(number))
        row += 1
        digits.append(number)
        if len(digits) == 3: # 3 digits each column
            #print("column with 3 digits: {0}".format(digits))
            # Check for duplicates by counting (count()) number
            # of times a number is found in the list and reduce
            # them using set()
            duplicates = set([str(n) for n in digits if digits.count(n) > 1])
            #print("duplicates: {0}".format(duplicates))
            if len(duplicates) > 0:
                result = False
                duplicates = ','.join(list(duplicates))    
                #print("duplicates: {0}".format(duplicates))

                column_key = column + 1 # +1 because to specify a real row number
                if column_key not in repeated['columns3x3']:
                    repeated['columns3x3'][column_key] = { 'repeated': [] }

                repeated['columns3x3'][column_key]['repeated'].append(duplicates)

            del digits[:]

        if row >= NUM_ROWS:
            # Reset row
            row = 0
            # and jump to the next column
            column += 1
            #print("Jumping to the next column: {0}".format(column))            
        if column >= NUM_COLUMNS:
            break

    return result

def show_report(repeated):
    """ Print contents of 'repeated' dictionary """

    print("Rows")
    print("*"*50)
    sorted_keys = sorted(list(repeated['rows'].keys()))
    for key in sorted_keys:
        print("Repeated digits in row {0} -> {1}".format(
                key, repeated['rows'][key]['repeated'])
        )

    print("Columns")
    print("*"*50)        
    sorted_keys = sorted(list(repeated['columns'].keys()))    
    for key in sorted_keys:
        print("Repeated digits in column {0} -> {1}".format(
                key, repeated['columns'][key]['repeated'])
        )

    print("Rows 3x3")
    print("*"*50)        
    sorted_keys = sorted(list(repeated['rows3x3'].keys()))    
    for key in sorted_keys:
        print("Repeated digits in row3x3 {0} -> {1}".format(
                key, repeated['rows3x3'][key]['repeated'])
        )       

    print("Columns 3x3")
    print("*"*50)        
    sorted_keys = sorted(list(repeated['columns3x3'].keys()))    
    for key in sorted_keys:
        print("Repeated digits in columns3x3 {0} -> {1}".format(
                key, repeated['columns3x3'][key]['repeated'])
        )      

def validate_sudoku(numbers, repeated):
    """ Search for repeated digits """

    # If any of this items is set to False
    # then the sudoku is not valid
    results = [True, True, True, True]

    results[0] = validate_rows(numbers, repeated)
    results[1] = validate_columns(numbers, repeated)
    results[2] = validate_rows_3x3(numbers, repeated)
    results[3] = validate_columns_3x3(numbers, repeated)            
    
    return results

if __name__ == "__main__":

    # Optional: only if you want to generate sudocus
    #generate_sudoku()

    filename = input("File containing 9x9 matrix: ")
    if not os.path.exists(filename):
        print("Error: file '{0}' does not exist".format(filename))
        sys.exit()

    numbers = load_sudoku(filename)
    if not numbers:
        print("Error: Cannot load this file")
        sys.exit()

    # Numbers were loaded as strings and must be
    # converted to integer
    numbers = [list(map(int, row)) for row in numbers]

    # Show sudoku values
    for row in range(NUM_COLUMNS):
        print(numbers[row])

    # Initialize dictionary for saving reports
    repeated = { 
        'rows': {}, 
        'columns': {}, 
        'rows3x3': {},
        'columns3x3': {}
    }

    results = validate_sudoku(numbers, repeated)
    print("\nValidations: {0}".format(results))
    # Check if four validations are correct
    if results.count(True) == 4:
        print("sudoku is valid!")
        # Create a copy of specified txt file with prefix VALID_
        with open(filename, "r") as file_hander:
            lines = file_hander.readlines()
        with open("VALID_{0}".format(filename), "w") as file_hander:
            file_hander.writelines(lines)
    else:
        print("*** Not a valid sudoku")
        show_report(repeated)